package by.itacademy.moiseenkolydia.webdriver;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {
    @Test
    public void testOnlinerOpen() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.onliner.by/");

        // обращение к web элементу лого
        String logoXpath = "//*[@class='onliner_logo']";
        By byLogo = By.xpath(logoXpath);
        WebElement logoElement = driver.findElement(byLogo);
        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }
    @Test
    public void testAmazonOpen() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");

        String logoXpath = "/html/body/div[1]/header/div/div[1]/div[1]/div[1]/a";
        By byLogo = By.xpath(logoXpath);
        WebElement logoElement = driver.findElement(byLogo);
        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }
    @Test
    public void testTicketproOpen() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.ticketpro.by/");

        String logoXpath = "/html/body/div[2]/header/div/div[1]/div[1]/div/a/img";
        By byLogo = By.xpath(logoXpath);
        WebElement logoElement = driver.findElement(byLogo);
        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }
    @Test
    public void testAlatantourOpen() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://alatantour.by/");

        String logoXpath = "/html/body/header/div/div/div/div/a/img";
        By byLogo = By.xpath(logoXpath);
        WebElement logoElement = driver.findElement(byLogo);
        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }
    @Test
    public void testOlxOpen() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.olx.pl/");

        String logoXpath = "//*[@id=\"headerLogo\"]";
        By byLogo = By.xpath(logoXpath);
        WebElement logoElement = driver.findElement(byLogo);
        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }
    @Test
    public void testTripadvisorOpen() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.tripadvisor.com/");

        String logoXpath = "//*[@id=\"lithium-root\"]/header/div/nav/h1/picture/img";
        By byLogo = By.xpath(logoXpath);
        WebElement logoElement = driver.findElement(byLogo);
        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }
}
